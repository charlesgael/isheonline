import Reflux from 'reflux';
import mkdirp from 'mkdirp';
import path from 'path';
import { readFileSync, writeFileSync } from 'jsonfile';
import iziToast from 'izitoast';
import Actions from './actions';

export default class ConfigStore extends Reflux.Store {
    constructor() {
        super();
        this.state = {
            access_token: '',
            guild_selected: '',
            user_tracked: '',
            config_loading: 0,
        };
        this.listenToMany(Actions);

        this.configPath = require('persist-path')('isheonline/config.json');

        this.load();
    }

    load() {
        try {
            this.data = readFileSync(this.configPath);
            if (this.data.access_token) {
                this.state.access_token = this.data.access_token;
            }
            if (this.data.channel_selected) {
                this.state.channel_selected = this.data.channel_selected;
            }
            if (this.data.user_tracked) {
                this.state.user_tracked = this.data.user_tracked;
            }
        } catch (e) {
            this.data = {};
        }
    }

    save() {
        try {
            mkdirp.sync(path.dirname(this.configPath));
            writeFileSync(this.configPath, this.data, { spaces: 2 });
        } catch (e) {
            iziToast.error({
                message: 'Error while saving config: ' + e.reason,
            });
        }
    }

    forceUpdate() {
        this.setState(this.state);
    }

    onChangeAccessToken() {
        this.setState({ config_loading: 1 });
    }

    onChangeAccessTokenCompleted(token) {
        this.data.access_token = token;
        this.save();

        iziToast.success({
            title: 'Connection complete',
            message: 'Access token tested and saved',
        });

        this.setState({
            config_loading: 0,
            access_token: token,
        });
    }

    onChangeAccessTokenFailed(error) {
        iziToast.error({
            title: 'Error',
            message: error,
        });

        this.setState({ config_loading: 0 });
    }

    onSelectGuild(guildId) {
        this.data.channel_selected = guildId;
        this.save();

        iziToast.success({
            message: 'Channel saved',
        });

        this.setState({
            channel_selected: guildId,
        });
    }

    onSelectUser(userId) {
        this.data.user_tracked = userId;
        this.save();

        iziToast.success({
            message: 'Who to track saved',
        });

        this.setState({
            user_tracked: userId,
        });
    }
}