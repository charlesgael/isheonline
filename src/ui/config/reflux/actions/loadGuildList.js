import Discord from 'discord.js';
import Reflux from 'reflux';

const loadGuildList = Reflux.createAction({ asyncResult: true });

loadGuildList.listen(function (token) {
    const client = new Discord.Client();

    client.on('ready', () => {
        const guilds = client.guilds.map(guild => ({
            id: guild.id,
            name: guild.name,
            channels: guild.channels.filter(channel => channel.type === 'voice').map(channel => ({
                id: channel.id,
                name: channel.name,
            })),
            members: guild.members.map(member => ({
                id: member.id,
                name: member.nickname || (member.user && member.user.username),
            })),
        }));

        client.destroy()
            .catch(() => {
                this.failed('No connection to internet');
            });
        this.completed(guilds);
    });
    client.on('voiceStateUpdate', console.log);

    client.login(token)
        .catch((e) => {
            this.failed(e.reason);
        });
});

module.exports = loadGuildList;