import Reflux from 'reflux';

const selectUser = Reflux.createAction();

module.exports = selectUser;