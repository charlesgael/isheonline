import Reflux from 'reflux';

const selectGuild = Reflux.createAction();

module.exports = selectGuild;