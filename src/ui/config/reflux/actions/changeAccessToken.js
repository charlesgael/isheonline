import Discord from 'discord.js';
import Reflux from 'reflux';

const changeAccessToken = Reflux.createAction({ asyncResult: true });

changeAccessToken.listen(function (token) {
    const client = new Discord.Client();

    client.on('ready', () => {
        client.destroy()
            .catch((e) => {
                console.log(e);
                this.failed('No connection to internet');
            });
        this.completed(token);
    });

    client.login(token)
        .catch((e) => {
            this.failed(e.message);
        });
});

module.exports = changeAccessToken;