import Reflux from 'reflux';
import iziToast from 'izitoast';
import ConfigStore from './config.store';
import Actions from './actions';

export default class GuildStore extends Reflux.Store {
    constructor() {
        super();
        this.state = {
            guild_loading: 1,
            guilds: null,
            users: null,
        };
        this.listenToMany(Actions);

        this.configStore = Reflux.initStore(ConfigStore);
        Actions.loadGuildList(this.configStore.state.access_token);
    }

    forceUpdate() {
        this.setState(this.state);
    }

    // eslint-disable-next-line class-methods-use-this
    onChangeAccessTokenCompleted(token) {
        Actions.loadGuildList(token);
    }

    onLoadGuildListFailed() {
        iziToast.error({
            title: 'Error',
            message: 'Could not log in using saved token please check it',
        });

        this.setState({
            guild_loading: 0,
        });
    }

    onLoadGuildListCompleted(guilds) {
        this.setState({
            guild_loading: 0,
            guilds,
        });
    }
}