import React from 'react';
import Reflux from 'reflux';
import { CustomInput, Form, FormGroup, Label, Progress } from 'reactstrap';
import GuildStore from '../reflux/guild.store';
import ConfigStore from '../reflux/config.store';
import Actions from '../reflux/actions';

export default class GuildSelection extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.stores = [ConfigStore, GuildStore];

        // BINDINGS
        this.renderChannel = this.renderChannel.bind(this);
        this.renderGuild = this.renderGuild.bind(this);
        this.changeChannel = this.changeChannel.bind(this);
    }

    renderChannel(channel) {
        return (
            <FormGroup key={channel.id} check>
                <Label check>
                    <CustomInput
                        type="radio"
                        id={'channel_' + channel.id}
                        name="channel_selection"
                        value={channel.id}
                        label={channel.name}
                        onChange={this.changeChannel}
                        checked={this.state.channel_selected === channel.id}
                    />
                </Label>
            </FormGroup>
        );
    }

    renderGuild(guild) {
        return (
            <FormGroup key={guild.id} tag="fieldset">
                <legend>{guild.name}</legend>

                {guild.channels && guild.channels.map(this.renderChannel)}
            </FormGroup>
        );
    }

    render() {
        return (
            <div className="container">
                {this.state.guild_loading > 0 &&
                <Progress animated value={100}/>
                }

                <Form>
                    {this.state.guilds && this.state.guilds.map(this.renderGuild)}
                </Form>
            </div>
        );
    }

    changeChannel(e) {
        Actions.selectGuild(e.target.value);
        this.forceUpdate();
    }
}