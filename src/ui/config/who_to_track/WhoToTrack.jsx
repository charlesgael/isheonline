import React from 'react';
import Reflux from 'reflux';
import { CustomInput, Form, FormGroup, Label, Progress } from 'reactstrap';
import ConfigStore from '../reflux/config.store';
import GuildStore from '../reflux/guild.store';
import Actions from '../reflux/actions';

export default class WhoToTrack extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.stores = [ConfigStore, GuildStore];

        // BINDINGS
        this.renderUser = this.renderUser.bind(this);
        this.renderGuild = this.renderGuild.bind(this);
        this.changeUser = this.changeUser.bind(this);
    }

    renderUser(user) {
        return (
            <FormGroup key={user.id} check>
                <Label check>
                    <CustomInput
                        type="radio"
                        id={'user_' + user.id}
                        name="user_selection"
                        value={user.id}
                        label={user.name}
                        onChange={this.changeUser}
                        checked={this.state.user_tracked === user.id}
                    />
                </Label>
            </FormGroup>
        );
    }

    renderGuild(guild) {
        return (
            <FormGroup key={guild.id} tag="fieldset">
                <legend>{guild.name}</legend>

                {guild.members && guild.members.map(this.renderUser)}
            </FormGroup>
        );
    }

    render() {
        return (
            <div className="container">
                {this.state.guild_loading > 0 &&
                <Progress animated value={100}/>
                }

                <Form>
                    {this.state.guilds && this.state.guilds.map(this.renderGuild)}
                </Form>
            </div>
        );
    }

    changeUser(e) {
        Actions.selectUser(e.target.value);
        this.forceUpdate();
    }
}