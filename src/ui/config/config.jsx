import React from 'react';
import { render } from 'react-dom';
import { HashRouter, Route, Switch, withRouter } from 'react-router-dom';
import { Nav, NavItem, NavLink } from 'reactstrap';
import Reflux from 'reflux';
import GuildSelection from './guild_selection';
import WhoToTrack from './who_to_track';
import AccessToken from './access_token';
import ConfigStore from './reflux/config.store';
import GuildStore from './reflux/guild.store';

Reflux.initStore(ConfigStore);
Reflux.initStore(GuildStore);

class App extends Reflux.Component {
    constructor(props) {
        super(props);

        this.store = ConfigStore;
    }

    render() {
        const { location } = this.props;

        return (
            <div>
                <div className="app-navbar">
                    <Nav pills justified>
                        <NavItem>
                            <NavLink
                                href="#/"
                                active={location.pathname === '/'}
                            >Access token</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                href="#/guild_selection"
                                active={location.pathname === '/guild_selection'}
                                disabled={this.state.access_token === ''}
                            >Guild selection</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                href="#/who_to_track"
                                active={location.pathname === '/who_to_track'}
                                disabled={this.state.access_token === '' || this.state.channel_selected === ''}
                            >Who to track</NavLink>
                        </NavItem>
                    </Nav>
                </div>

                <div className="main-container">
                    <Switch>
                        <Route exact path="/" component={AccessToken}/>
                        <Route exact path="/guild_selection" component={GuildSelection}/>
                        {<Route exact path="/who_to_track" component={WhoToTrack}/>}
                    </Switch>
                </div>
            </div>
        );
    }
}

const AppRouted = withRouter(App);


render(
    (
        <HashRouter>
            <AppRouted/>
        </HashRouter>
    ),
    document.getElementById('app'));