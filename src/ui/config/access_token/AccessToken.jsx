import React from 'react';
import Reflux from 'reflux';
import { Button, Form, FormGroup, Input, InputGroup, InputGroupAddon, Label, Progress } from 'reactstrap';
import ConfigStore from '../reflux/config.store';
import Actions from '../reflux/actions';

export default class AccessToken extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {
            field: {
                access_token: '',
            },
        };

        this.store = ConfigStore;

        this.mapStoreToState(ConfigStore, fromStore => ({
            field: {
                access_token: fromStore.access_token,
            },
        }));

        // BINDINGS
        this.fieldChanged = this.fieldChanged.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    componentDidMount() {
    }

    render() {
        return (
            <div className="container">
                <Form onSubmit={this.handleFormSubmit}>
                    <FormGroup>
                        <Label>Access Token</Label>
                        <InputGroup>
                            <Input
                                name="access_token"
                                type="text"
                                placeholder="Enter access token"
                                value={this.state.field.access_token}
                                onChange={this.fieldChanged}
                            />
                            <InputGroupAddon addonType="append">
                                <Button color="primary" type="submit" disabled={!this.isSubmitAvailable()}>Save</Button>
                            </InputGroupAddon>
                        </InputGroup>
                    </FormGroup>
                </Form>

                {this.state.config_loading > 0 &&
                <Progress animated value={100}/>
                }
            </div>
        );
    }

    /**
     * Called when a field is changed, used to verify form
     */
    fieldChanged(e) {
        this.state.field[e.target.name] = e.target.value.trim();
        this.forceUpdate();
    }

    /**
     * Called when test button is pressed
     * @param e Event object used to cancel default behaviour
     */
    handleFormSubmit(e) {
        e.preventDefault();
        const accessToken = this.state.field.access_token;
        Actions.changeAccessToken(accessToken);
    }

    /**
     * Check method used to know if test button can be pressed
     * @returns {boolean} true if button is enabled
     */
    isSubmitAvailable() {
        const accessToken = this.state.field.access_token;
        return this.state.config_loading === 0
            // validate access_token
            && accessToken !== ''
            && accessToken !== this.state.access_token;
    }
}