import { BrowserWindow } from 'electron';
// import { enableLiveReload } from 'electron-compile';
import installExtension, { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';

import * as url from 'url';
import * as path from 'path';

import * as icons from '../icons';

let mainWindow = null;

const isDevMode = process.execPath.match(/[\\/]electron/);

// if (isDevMode) {
//     enableLiveReload({ strategy: 'react-hmr' });
// }

export default async function openConfigUi() {
    if (mainWindow !== null) {
        mainWindow.focus();
    } else {
        mainWindow = new BrowserWindow({
            width: 640,
            height: 480,
            resizable: false,
            maximizable: false,
            title: 'Settings',
            autoHideMenuBar: true,
            icon: icons.base,
        });

        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, '../ui/config/index.html'),
            protocol: 'file',
            slashes: true,
        }));

        // Open the DevTools.
        if (isDevMode) {
            await installExtension(REACT_DEVELOPER_TOOLS);
            mainWindow.webContents.openDevTools();
        }

        mainWindow.on('closed', () => {
            mainWindow = null;
        });
    }
}

