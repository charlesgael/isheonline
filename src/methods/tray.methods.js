import { Menu, Tray } from 'electron';

import openConfigUi from './window.methods';
import * as icons from '../icons';

let tray;

export function setChannelUsers(users) {
    let config;

    if (!users.length) {
        config = [{ label: 'No users online' }];
    } else {
        config = users.map(user => ({
            label: user.name,
            icon: user.on ? icons.on : icons.absent,
        }));
    }

    tray.setContextMenu(Menu.buildFromTemplate(
        config.concat([
            { type: 'separator' },
            { label: 'Open configuration', click: openConfigUi },
            { label: 'Exit', role: 'quit' },
        ])),
    );
}

export function createTray() {
    tray = new Tray(icons.base);
    tray.setToolTip('IsHeOnline');

    setChannelUsers([]);

    tray.on('click', openConfigUi);

    return tray;
}

export function changeTray(icon, text) {
    if (icon) {
        tray.setImage(icon);
    }
    if (text) {
        tray.setToolTip(text);
    }
}