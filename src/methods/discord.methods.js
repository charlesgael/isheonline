import * as chokidar from 'chokidar';
import Discord from 'discord.js';
import * as jsonfile from 'jsonfile';
import * as icons from '../icons';
import { changeTray, setChannelUsers } from './tray.methods';

export default class DiscordConnector {
    constructor() {
        this.inner_configFile = null;
        this.watcher = null;
        this.client = null;
        this.timeout = null;

        // BINDING
        this.readConfigFile = this.readConfigFile.bind(this);
    }

    set configFile(configFile) {
        this.inner_configFile = configFile;
        this.readConfigFile();
        this.startWatching();
    }

    startWatching() {
        this.watcher = chokidar.watch(this.inner_configFile, { persistent: true });
        this.watcher
            .on('add', this.readConfigFile)
            .on('change', this.readConfigFile)
            .on('unlink', this.readConfigFile);
    }

    readConfigFile() {
        if (this.client != null) {
            this.client.destroy();
        }
        this.client = null;
        if (this.timeout != null) {
            clearTimeout(this.timeout);
        }
        this.timeout = null;

        try {
            const obj = jsonfile.readFileSync(this.inner_configFile);

            if (!obj.access_token) {
                changeTray(icons.error, 'No access token configured!');
                return;
            }
            if (!obj.channel_selected) {
                changeTray(icons.error, 'No channel selected!');
                return;
            }
            if (!obj.user_tracked) {
                changeTray(icons.base, 'No user to track selected!');
            }
            this.connect(obj);
        } catch (e) {
            console.log(e);
            if (e.code === 'ENOENT') {
                changeTray(icons.error, 'Not configured yet!');
            } else {
                changeTray(icons.error, 'Invalid configuration file!');
            }
        }
    }

    static updatePresence(conf, users) {
        let online = false;
        users.forEach((user) => {
            if (user.id === conf.user_tracked) {
                online = true;
                if (user.on) {
                    changeTray(icons.on, user.name, 'online');
                } else {
                    changeTray(icons.absent, user.name, 'absent');
                }
            }
        });
        if (!online) {
            changeTray(icons.off, ' offline');
        }
    }

    static getName(member) {
        return member.nickname || (member.user && member.user.username);
    }

    static getStatus(member) {
        return !member.selfDeaf && !member.selfMute && member.presence.status === 'online';
    }

    connect(conf) {
        this.client = new Discord.Client();

        const client = this.client;
        let users = [];

        client.on('disconnect', (e) => {
            changeTray(icons.error, e.reason);
        });
        client.on('error', () => {
            changeTray(icons.error, 'Error on socket connect');
        });

        const initFun = () => {
            users = client.channels.get(conf.channel_selected).members.map(member => ({
                id: member.id,
                name: DiscordConnector.getName(member),
                on: DiscordConnector.getStatus(member),
            }));
            DiscordConnector.updatePresence(conf, users);
            setChannelUsers(users);
        };
        client.on('ready', initFun);
        client.on('resume', initFun);

        const changeFun = (oldMember, newMember) => {
            const user = {
                id: newMember.id,
                name: DiscordConnector.getName(newMember),
                on: DiscordConnector.getStatus(newMember),
            };

            if (oldMember.voiceChannelID !== conf.channel_selected
                && newMember.voiceChannelID === conf.channel_selected) { // connection
                console.log('[+]', user.name);
                users.push(user);
            } else {
                const index = users.findIndex(u => u.id === newMember.id);
                if (index !== -1) {
                    const oldUser = users[index];

                    if (oldMember.voiceChannelID === conf.channel_selected
                        && newMember.voiceChannelID !== conf.channel_selected) { // disconnection
                        console.log('[-]', user.name);
                        users.splice(users.findIndex(u => u.id === newMember.id), 1);
                    } else if (user.name !== oldUser.name
                        || user.on !== oldUser.on) {
                        console.log('[~]', user.name, '(' + user.on + ')');
                        users[index] = user;
                    }
                }
            }
            console.log(users);
            DiscordConnector.updatePresence(conf, users);
            setChannelUsers(users);
        };
        client.on('voiceStateUpdate', changeFun);
        client.on('presenceUpdate', changeFun);

        client.login(conf.access_token)
            .catch(() => {
                changeTray(icons.error, 'Error on socket connect');
                this.timeout = setTimeout(this.readConfigFile, 30 * 1000);
            });
    }
}