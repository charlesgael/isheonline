import { app } from 'electron';
import openConfigUi from './methods/window.methods';
import DiscordConnector from './methods/discord.methods';
import { createTray } from './methods/tray.methods';

(() => {
    if (require('electron-squirrel-startup')) return;

    app.on('ready', () => {
        createTray();
        const connector = new DiscordConnector();
        connector.configFile = require('persist-path')('isheonline/config.json');
    });

    app.on('activate', openConfigUi);
    app.on('window-all-closed', e => e.preventDefault());
})();