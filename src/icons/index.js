import * as path from 'path';

export const off = path.join(__dirname, 'off.png');
export const on = path.join(__dirname, 'on.png');
export const absent = path.join(__dirname, 'absent.png');
export const base = path.join(__dirname, 'icon.png');
export const error = path.join(__dirname, 'error.png');